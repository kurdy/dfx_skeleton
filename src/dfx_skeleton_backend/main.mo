import Text "mo:base/Text";
import Principal "mo:base/Principal";
import Array "mo:base/Array";
import Iter "mo:base/Iter";
import Debug "mo:base/Debug";
import Result "mo:base/Result";
import Nat "mo:base/Nat";
import TrieSet "mo:base/TrieSet";
import Time "mo:base/Time";
import ExperimentalCycles "mo:base/ExperimentalCycles";

shared({caller = canisterOwner}) actor class Main(){

  type Result<T,E> = Result.Result<T,E>;

  stable var allowedStore : [Principal] = [];
  var allowed = TrieSet.empty<Principal>();  

  system func postupgrade() {
      Debug.print ("Call of Main:postupgrade.");
      allowed:=TrieSet.fromArray<Principal>(allowedStore,Principal.hash,Principal.equal);
      allowedStore:=[];
      if(TrieSet.isEmpty(allowed)){
        allowed:=TrieSet.put<Principal>(
          allowed,
          canisterOwner,
          Principal.hash(canisterOwner),
          Principal.equal);
      }

  };

  system func preupgrade() {
      Debug.print ("Call of Main:preupgrade.");
      allowedStore := TrieSet.toArray(allowed);
  };

  public query func ping(timestamp : Nat) : async Nat {
    return timestamp;
  };

  public shared (msg) func getCanisterOwner() : async Result<Principal,Text> {
    if (not Principal.isAnonymous(msg.caller)) {
      #ok(canisterOwner);
    } else {
      #err("Shouldn't be a annonymous caller.");
    }
  };

  public shared (msg) func whoami() : async Text {
      Principal.toText(msg.caller);
  };

  public shared (msg) func isAnonymous() : async Bool {
      Principal.isAnonymous(msg.caller);
  };

  public shared (msg) func getAllowed() : async Result<[Text],Text> {
    if(Principal.equal(canisterOwner,msg.caller)){
      #ok(Array.map<Principal,Text>(TrieSet.toArray<Principal>(allowed), func(i) {Principal.toText(i)}));
    } else {
      #err("Error: Only canister owner can call this function.");
    }
  };

  public shared (msg) func allow(id:Text) : async Result<(),Text> {
    if(Principal.equal(canisterOwner,msg.caller)){
      let p : Principal = Principal.fromText(id);
      allowed:=TrieSet.put<Principal>(
          allowed,
          p,
          Principal.hash(p),
          Principal.equal);
      #ok 
    } else {
      #err("Error: Only canister owner can call this function.");
    }
    
  };

  public query func canisterTime() : async Int {
    Time.now()/1_000_000;
  };

  public shared (msg)  func getCyclesBalance () : async ?Nat {
    if (not Principal.isAnonymous(msg.caller)) {
      return ?ExperimentalCycles.balance()
    } else {
      return null
    }
  };

   public shared (msg)  func getCyclesAvailable () : async ?Nat {
    if (not Principal.isAnonymous(msg.caller)) {
      return ?ExperimentalCycles.available()
    } else {
      return null
    }
  };

};
