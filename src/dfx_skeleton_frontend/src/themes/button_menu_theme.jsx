import React,{Fragment,useRef} from "react";
import { Menu } from 'primereact/menu';
import { Button } from 'primereact/button';
import {useIntl} from 'react-intl';
import PropTypes from "prop-types";


function MenuThemeSelector({ icon }) {

    const intl = useIntl();

    const buttonIcon = icon || 'pi pi-ellipsis-v';

    const onThemeChange = (theme) => {
        localStorage.setItem("app.theme",theme);
        window.location.reload(true);
    };

    const menuTheme = useRef(null);

    const themes = [
        {
          label: "Light Blue", 
          command: () => {onThemeChange('bootstrap4-light-blue');}
        },
        {
          label: "Dark Blue", 
          command: () => {onThemeChange('bootstrap4-dark-blue');}
        },
        {
          label: "Lara Light Indigo", 
          command: () => {onThemeChange('lara-light-indigo');}
        },
        {
          label: "Lara Dark Indigo", 
          command: () => {onThemeChange('lara-dark-indigo');}
        },
        {
          label: "Lara Light Teal", 
          command: () => {onThemeChange('lara-light-teal');}
        },
        {
          label: "Lara Dark Teal", 
          command: () => {onThemeChange('lara-dark-teal');}
        },
        {
          label: "Luna Pink", 
          command: () => {onThemeChange('luna-pink');}
        },
        {
          label: "Saga Orange", 
          command: () => {onThemeChange('saga-orange');}
        }
    ];

    return (
        <Fragment>
          <Menu model={themes} popup ref={menuTheme} id="popup_theme" />
          <Button icon={buttonIcon} className="p-button-rounded p-button-text text-color-secondary" onClick={(event) => menuTheme.current.toggle(event)} tooltip={intl.formatMessage({ id: 'key.tooltip.menu.theme',defaultMessage: 'Select a theme'})} tooltipOptions={{position: 'left'}} aria-controls="popup_theme" aria-haspopup/>
        </Fragment>
    )
}

MenuThemeSelector.propTypes = {
  icon: PropTypes.string

}

export default MenuThemeSelector