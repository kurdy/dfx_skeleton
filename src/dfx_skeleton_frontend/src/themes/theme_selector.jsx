import React, { Suspense,Fragment } from 'react';
import PropTypes from "prop-types";

{/* Inspired from https://prawira.medium.com/react-conditional-import-conditional-css-import-110cc58e0da6 

primereact/resources/themes/bootstrap4-light-blue/theme.css
primereact/resources/themes/bootstrap4-light-purple/theme.css
primereact/resources/themes/bootstrap4-dark-blue/theme.css
primereact/resources/themes/bootstrap4-dark-purple/theme.css
primereact/resources/themes/md-light-indigo/theme.css
primereact/resources/themes/md-light-deeppurple/theme.css
primereact/resources/themes/md-dark-indigo/theme.css
primereact/resources/themes/md-dark-deeppurple/theme.css
primereact/resources/themes/mdc-light-indigo/theme.css
primereact/resources/themes/mdc-light-deeppurple/theme.css
primereact/resources/themes/mdc-dark-indigo/theme.css
primereact/resources/themes/mdc-dark-deeppurple/theme.css
primereact/resources/themes/fluent-light/theme.css
primereact/resources/themes/lara-light-blue/theme.css
primereact/resources/themes/lara-light-indigo/theme.css
primereact/resources/themes/lara-light-purple/theme.css
primereact/resources/themes/lara-light-teal/theme.css
primereact/resources/themes/lara-dark-blue/theme.css
primereact/resources/themes/lara-dark-indigo/theme.css
primereact/resources/themes/lara-dark-purple/theme.css
primereact/resources/themes/lara-dark-teal/theme.css
primereact/resources/themes/saga-blue/theme.css
primereact/resources/themes/saga-green/theme.css
primereact/resources/themes/saga-orange/theme.css
primereact/resources/themes/saga-purple/theme.css
primereact/resources/themes/vela-blue/theme.css
primereact/resources/themes/vela-green/theme.css
primereact/resources/themes/vela-orange/theme.css
primereact/resources/themes/vela-purple/theme.css
primereact/resources/themes/arya-blue/theme.css
primereact/resources/themes/arya-green/theme.css
primereact/resources/themes/arya-orange/theme.css
primereact/resources/themes/arya-purple/theme.css
primereact/resources/themes/nova/theme.css
primereact/resources/themes/nova-alt/theme.css
primereact/resources/themes/nova-accent/theme.css
primereact/resources/themes/luna-amber/theme.css
primereact/resources/themes/luna-blue/theme.css
primereact/resources/themes/luna-green/theme.css
primereact/resources/themes/luna-pink/theme.css
primereact/resources/themes/rhea/theme.css


*/}

const BootstrapLightBlue = React.lazy(() => import('./bootstrap4_light_blue'));
const BootstrapDarkBlue = React.lazy(() => import('./bootstrap4_dark_blue'));
const LaraDarkTeal = React.lazy(() => import('./lara_dark_teal'));
const LaraLightTeal = React.lazy(() => import('./lara_light_teal'));
const LaraLightIndigo = React.lazy(() => import('./lara_light_indigo'));
const LaraDarkIndigo = React.lazy(() => import('./lara_dark_indigo'));
const LunaPink = React.lazy(() => import('./luna_pink'));
const SagaOrange = React.lazy(() => import('./saga_orange'));

import { ProgressSpinner } from 'primereact/progressspinner';

const ThemeSelector = ({ children }) => {
    const theme = localStorage.getItem('app.theme') || 'lara-light-indigo';
    return (
      <Fragment>
        <Suspense fallback={<div className="flex justify-content-start align-items-center min-h-screen"><ProgressSpinner/></div>}>
          {(theme === 'bootstrap4-light-blue') && <BootstrapLightBlue />}
          {(theme === 'bootstrap4-dark-blue') && <BootstrapDarkBlue />}
          {(theme === 'lara-dark-teal') && <LaraDarkTeal />}
          {(theme === 'lara-light-teal') && <LaraLightTeal />}
          {(theme === 'lara-light-indigo') && <LaraLightIndigo />}
          {(theme === 'lara-dark-indigo') && <LaraDarkIndigo />}
          {(theme === 'luna-pink') && <LunaPink />}
          {(theme === 'saga-orange') && <SagaOrange />}
        </Suspense>
        {children}
      </Fragment>
    )
  }

  ThemeSelector.propTypes= {
    children: PropTypes.node
  
  }

  export default ThemeSelector;