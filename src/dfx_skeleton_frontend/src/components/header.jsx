import React from 'react';
import { FormattedMessage} from "react-intl";
import Logo from "./logo.jsx";
import MenuThemeSelector from "../themes/button_menu_theme";

function Header() {
    return (
            <div className="flex flex-row bg-primary">
                <div className='flex flex-grow-0 align-items-center m-2'>
                    <Logo size={'3rem'} bcolor={'var(--primary-color-text)'} fcolor={'var(--primary-color)'}/>
                </div>
                <div className='flex flex-grow-1 justify-content-center'>
                    <div className="flex flex-column bg-primary">
                        <div className='flex text-3xl mt-3 justify-content-center text-color mt-2'>
                            <FormattedMessage defaultMessage='Home' id='component.home.name'/> 
                        </div>
                        <div className='hidden sm:flex text-1xl mt-1 justify-content-center text-color mb-2'>
                            <FormattedMessage defaultMessage='Skeleton for Internet Computer, Internet Computer Identity and Primereact' id='component.home.description'/>
                        </div>
                    </div>   
                </div>
                <div className='flex flex-grow-0 align-items-center m-2'>
                    <MenuThemeSelector/>
                </div>
            </div>
    );
}

export default Header;