[![pipeline status](https://gitlab.com/kurdy/dfx_skeleton/badges/main/pipeline.svg)](https://gitlab.com/kurdy/dfx_skeleton/-/commits/main)
# dfx_skeleton

The purpose of this project is to have an up-to-date application skeleton that includes:

* DFINITY Web3 Internet compter *dfx*
* DFINITY Internet Computer Identity
* [authenticator NFID](https://nfid.one/) 
* primereact
* react
* react-hook-form
* react-intl
* react-router-dom
* esLint

~~Online version~~: 
* ~~[https://hdjs4-yyaaa-aaaak-ab5ea-cai.icp0.io/](https://hdjs4-yyaaa-aaaak-ab5ea-cai.icp0.io/)~~
* ~~[Custom domain skeleton.kurdy.org](https://skeleton.kurdy.org)~~

## Try using gitpod

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/kurdy/dfx_skeleton)

> **Note** The first launch may take a while

To setup Internet-Identity:
* In the INTERNET_IDENTITY terminal 
* Take the id of the canister internet_identity
* In the DFX_SKELETON terminal
    * Open the browser
    * Get the https address of your node
        * Example: https://4943-kurdy-dfxskeleton-jfxpmfc24jl.ws-eu77.gitpod.io/?canisterId=<ID canister front_end>
* Create an .env file
* Update the connection url with canister id of internet_identity
    * Example: LOCAL_II_CANISTER="https://4943-kurdy-dfxskeleton-jfxpmfc24jl.ws-eu77.gitpod.io/?canisterId=<ID internet_identity>"

## local dev

If you want to start working on your project right away, you might want to try the following commands:

```bash
cd dfx_skeleton/
dfx help
dfx canister --help
```

## Running the project locally

If you want to test your project locally, you can use the following commands:

```bash
# Starts the replica, running in the background
dfx start --background

# Deploys your canisters to the replica and generates your candid interface
dfx deploy
```

Once the job completes, your application will be available at `http://localhost:4943?canisterId={asset_canister_id}`.

Additionally, if you are making frontend changes, you can start a development server with

```bash
npm start
```

Which will start a server at `http://localhost:8080`, proxying API requests to the replica at port 4943.

## call remote method

* `dfx canister --ic call <backend canister id> allow '("<principal id>")'`
* `dfx canister --ic call <backend canister id> getAllowed`


## Howto

* [PrimeReact](https://www.primefaces.org/primereact/)
* [Motoko](https://internetcomputer.org/docs/current/developer-docs/build/languages/motoko/)
* [Deploy](https://internetcomputer.org/docs/current/developer-docs/quickstart/network-quickstart)
* [DFinity Internet Compter](https://internetcomputer.org/)
* Internet Identity
    * [auth-client-demo](https://github.com/krpeacock/auth-client-demo)
    * [Internet Identity](https://github.com/dfinity/internet-identity)
    * [Videos](https://www.youtube.com/c/DFINITY)
* [authenticator NFID](https://nfid.one/)