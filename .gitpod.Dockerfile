# https://hub.docker.com/u/gitpod/ images
FROM gitpod/workspace-node-lts:latest

USER root
RUN apt-get update 
RUN apt-get install -y rsync curl 
RUN apt-get clean && rm -rf /var/cache/apt/* && rm -rf /var/lib/apt/lists/* && rm -rf /tmp/*

USER gitpod
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
RUN sh -ci "$(curl -fsSL https://internetcomputer.org/install.sh)"
RUN echo "export PATH=\"\$PATH:/home/gitpod/bin\"" >> /home/gitpod/.bashrc
RUN echo "export DFX_VERSION=\"0.14.2\"" >> /home/gitpod/.bashrc

SHELL ["/bin/bash"]

WORKDIR /home/gitpod/